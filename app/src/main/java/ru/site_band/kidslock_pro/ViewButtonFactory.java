package ru.site_band.kidslock_pro;

import android.content.Context;

/**
 * Created by KupriyanovYM on 16.02.2017.
 */

class ViewButtonFactory {

    private Context ctx;

    ViewButtonFactory(Context ctx){
        this.ctx = ctx;
    };

    public ViewButton createViewButton(int type) throws Exception{
        if (type == 0){
            return new ViewTextButton(ctx);
        }
        else if (type == 1){
            return new ViewImageButton(ctx);
        }
        throw new Exception("Button type not found");
    }
}
