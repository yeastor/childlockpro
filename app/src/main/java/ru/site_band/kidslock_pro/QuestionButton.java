package ru.site_band.kidslock_pro;

import android.content.Context;
import android.view.View;

/**
 * Created by KupriyanovYM on 03.02.2017.
 */

class QuestionButton {

    private ViewButton button;
    private Boolean wrongFlag;



    private String text;

    QuestionButton(int type, Context ctx){
        try {
            this.button = new ViewButtonFactory(ctx).createViewButton(type);
        }catch (Exception e){}


    }
    public void setContent(int resourceId){
        this.button.setContentFromResource(resourceId);
    }

    public void setId(int id){
        this.button.setId(id);
    }

    public void setSize(int width){
        this.button.setSize(width);
    }

    public void setWrongFlag(Boolean wrongFlag){
        this.wrongFlag = wrongFlag;
    }
    public void setText(String text){
        this.text = text;
    }

    public View getViewButton() {
        return button.getView();
    }

    public Boolean isWrong() {
        return wrongFlag;
    }
    public String getText() {
        return text;
    }






}
