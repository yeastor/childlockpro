package ru.site_band.kidslock_pro;

import android.content.Context;
import android.view.View;

/**
 * Created by KupriyanovYM on 16.02.2017.
 */

interface ViewButton {

     View getView();
     void setContentFromResource(int resourceId);
     void setId(int id);
     void setSize(int width);
}
