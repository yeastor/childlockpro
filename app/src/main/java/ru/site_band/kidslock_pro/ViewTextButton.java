package ru.site_band.kidslock_pro;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;

/**
 * Created by KupriyanovYM on 16.02.2017.
 */

class ViewTextButton implements ViewButton {

    private Button btn;
    private Context ctx;

    ViewTextButton(Context ctx){
        this.ctx = ctx;
        LayoutInflater ltInflater = LayoutInflater.from(ctx);
        this.btn = (Button) ltInflater.inflate(R.layout.button_letter_template, null, false);
    }

    public void setId(int id){
        this.btn.setId(id);
    }

    public View getView(){
        return this.btn;
    }

    public void setContentFromResource(int resourceId){
        String[] letterArray =  this.ctx.getResources().getStringArray(R.array.abc_what_learn);
        this.btn.setText(letterArray[resourceId]);
    }
    public void setSize(int width){
            GridLayout.LayoutParams llp = new GridLayout.LayoutParams();
            llp.width =  width;
            llp.height = width;
            llp.setMargins(10, 10, 10, 0);
            btn.setTextSize(TypedValue.COMPLEX_UNIT_PX, (width / Settings.FONT_BIG));
            btn.setLayoutParams(llp);
    }
}
