package ru.site_band.kidslock_pro;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageButton;

/**
 * Created by KupriyanovYM on 16.02.2017.
 */

class ViewImageButton implements ViewButton {
    private ImageButton btn;
    private Context ctx;

    ViewImageButton(Context ctx){
        this.ctx = ctx;
        LayoutInflater ltInflater = LayoutInflater.from(ctx);
        this.btn = (ImageButton) ltInflater.inflate(R.layout.image_buttom_template, null, false);
    }

    public View getView(){
        return this.btn;
    }

    public void setId(int id){
        this.btn.setId(id);
    }

    public void setContentFromResource(int resourceId){

        btn.setImageResource(getLetterImageResource(resourceId));
    }
    public void setSize(int width){
        GridLayout.LayoutParams llp = new GridLayout.LayoutParams();
        llp.width =  width;
        llp.height = width;
        llp.setMargins(10, 10, 10, 0);
        btn.setLayoutParams(llp);
    }

    private int getLetterImageResource(int index){
        TypedArray images = this.ctx.getResources().obtainTypedArray(R.array.letter_image_res);
        return  images.getResourceId(index - 1,0);
    }
}
