package ru.site_band.kidslock_pro;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.View;

import android.view.WindowManager;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import ru.site_band.kidslocklibrary.Controll;
import ru.site_band.kidslocklibrary.Screen;


public class LockScreenService extends Service implements View.OnClickListener {


    private LinearLayout linearLayout;
    private WindowManager.LayoutParams layoutParams;
    private WindowManager windowManager;
    private GridLayout glButtonsLayout;
    private int wrongClickCount = 0;
    private int maxWrongClickCount;
    Handler handler;
    Vibrator vibe;
    Locker locker;

    Handler h = new Handler();
    int delay = 15000; //milliseconds


    QuestionButtonsCollection questionButtonsCollection;
    @Override
    public IBinder onBind(Intent intent) {
        // Not used
        return null;
    }

    @Override
    public void onCreate() {
        handler = new Handler();
        super.onCreate();
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_USER_PRESENT);
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(screenReceiver, intentFilter);
        windowManager = ((WindowManager) getSystemService(WINDOW_SERVICE));
        layoutParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_ERROR,
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN //draw on status bar
                        | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION,// hiding the home screen button
                PixelFormat.TRANSLUCENT);


        setInterval();
    }


    private void init() {
        int lockType;
        LinearLayout llQuestionLayout;
        TextView tvWhatFind;

        if (!Screen.isScreenOn((PowerManager) getSystemService(POWER_SERVICE))){
            return;
        }
        vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        maxWrongClickCount = Settings.MAX_WRONG_CLICK_COUNT;
        linearLayout = new LinearLayout(this);
        windowManager.addView(linearLayout, layoutParams);
        ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.lock_screen, linearLayout);
        lockType = Settings.getSettings(this,Settings.LOCK_TYPE,1);
        glButtonsLayout = (GridLayout) linearLayout.findViewById(R.id.rrMain);
        llQuestionLayout = (LinearLayout)linearLayout.findViewById(R.id.llMain);
        tvWhatFind = (TextView) llQuestionLayout.findViewById(R.id.tvFindWhat);


        View.OnClickListener oclBtnWrong = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wrongClickCount++;
                vibe.vibrate(100);
                try {
                    SoundPlayer.playWrongSound(LockScreenService.this,locker.getTryMorePhraseResource());
                    disableQuestionButtons();
                    enableQuestionButtonsAfter(1700);
                }
                catch (Exception e){
                    e.printStackTrace();
                }


            }
        };


            try{
                locker = new LockerFabric().getLocker(LockScreenService.this,Screen.getScreenWidthInPx(windowManager),lockType);
                questionButtonsCollection = locker.getButtons();
                SoundPlayer.playSoundTwice(this,locker.getPreviewPhraseResource(),locker.getLetterSoundResource());
                for (int i=0; i < questionButtonsCollection.lenght(); i++){
                    QuestionButton button = questionButtonsCollection.get(i);
                    View btnView = button.getViewButton();
                    if (button.isWrong()) {
                        btnView.setOnClickListener(oclBtnWrong);
                    } else {
                        tvWhatFind.setText(String.format("%s %s",getResources().getString(locker.getWhatFindStringResource()),button.getText()));
                        btnView.setOnClickListener(this);
                    }
                    glButtonsLayout.addView(btnView);
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }


/*****************/




    }



    private void disableQuestionButtons(){
        Controll.cascadeDisableEnableControls(false, glButtonsLayout);
    }

    private void enableQuestionButtonsAfter(int millisecinds){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Controll.cascadeDisableEnableControls(true, glButtonsLayout);
                if (wrongClickCount == maxWrongClickCount){
                    vibe.vibrate(200);
                    redrawABCButtons(false);
                    wrongClickCount = 0;
                }
            }
        }, millisecinds);
    }


    private void  removeSheduller(){
        try{
            h.removeMessages(0);
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }


    private void redrawABCButtons(Boolean resize){
        //Helper.shuffleArray(btnIndexes);

        int width = 0;
        if (resize){
            width = Screen.getScreenWidthInPx(windowManager);
        }
        questionButtonsCollection = locker.redrawButtons(resize,width);
        SoundPlayer.playSoundTwice(LockScreenService.this,locker.getPreviewPhraseResource(),locker.getLetterSoundResource());
        glButtonsLayout.removeAllViews();
        int[] btnIndexes = locker.getBtnIndexes();
        for (int btnIndex: btnIndexes)     {
            glButtonsLayout.addView(questionButtonsCollection.get(btnIndex).getViewButton());
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (linearLayout == null) return;
        redrawABCButtons(true);
    }


    /**
     * if rigth click
     * @param view
     */
    @Override
    public void onClick(View view) {
        windowManager.removeView(linearLayout);
        linearLayout = null;
        setInterval();


    }

    private void setInterval() {
        int interValPos = Settings.getSettings(this, Settings.TIMER_INTERVAL, 0);

        if (interValPos == 0) {
            return;
        }
        String intVal = Settings.getSettingsString(this, Settings.TIMER_INTERVAL_VAL, "5");
        activateInterval(intVal);
    }

    private void activateInterval(String intVal){
        try {
            int intValMinutesInt = Integer.parseInt(intVal);
            sheduleNextRun(intValMinutesInt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sheduleNextRun(int minuteInterval){
        try{
            delay = minuteInterval * 60 * 1000;
            h.postDelayed(new Runnable(){
                public void run(){
                    //do something
                    init();
                }
            }, delay);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(screenReceiver);
        removeSheduller();
        super.onDestroy();
    }



    BroadcastReceiver screenReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_USER_PRESENT) && linearLayout == null) {
                init();
            }
            if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                removeSheduller();
            }
        }
    };






}
