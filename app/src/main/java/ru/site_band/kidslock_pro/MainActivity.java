package ru.site_band.kidslock_pro;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Switch;


public class MainActivity extends Activity {

    Switch swAppOnOff;
    Spinner spLockType;
    Spinner spInterval;
    ImageButton ibLockTypePref;
    int iCurrentSelection;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        swAppOnOff = (Switch) findViewById(R.id.swAppOnOff);
        spLockType = (Spinner) findViewById(R.id.spLockType);
        spInterval = (Spinner) findViewById(R.id.spInterval);
        ibLockTypePref = (ImageButton) findViewById(R.id.ibLockTypePref);
        loadSettings();
        if (swAppOnOff.isChecked()){
            if (!isMyServiceRunning(LockScreenService.class)){
                startLockService();
            }
        }
        iCurrentSelection = spInterval.getSelectedItemPosition();
        spInterval.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (iCurrentSelection != position){
                    try{
                        startStopServiceWithSheduler();
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                }
                iCurrentSelection = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        ibLockTypePref.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Меняем текст в TextView (tvOut)
                int lockType = spLockType.getSelectedItemPosition();
                if (lockType == 0 || lockType == 1){
                   openABCSettingsActivity();
               }
            }
            });

        swAppOnOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    saveSettings();
                    startLockService();
                } else {
                    stopService(new Intent(MainActivity.this, LockScreenService.class));
                }
            }
        });
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void startStopServiceWithSheduler(){
        if (!swAppOnOff.isChecked()) return;
        saveSettings();
        int intervalPos = spInterval.getSelectedItemPosition();
        String intervalVal = spInterval.getSelectedItem().toString();
        stopService(new Intent(MainActivity.this, LockScreenService.class));
        startService(new Intent(MainActivity.this, LockScreenService.class));


    }


    private void startLockService(){
        startService(new Intent(MainActivity.this, LockScreenService.class));
    };

    private void openABCSettingsActivity()   {
        Intent intent = new Intent(this, ABCSettingsActivity.class);
        startActivity(intent);
    }


    private void loadSettings(){
        Boolean appIsOn = Settings.getSettings(MainActivity.this, Settings.APP_ACTIVE,false);
        swAppOnOff.setChecked(appIsOn);
        spLockType.setSelection(Settings.getSettings(MainActivity.this,Settings.LOCK_TYPE,0));
        spInterval.setSelection(Settings.getSettings(MainActivity.this,Settings.TIMER_INTERVAL,0));
    }

    private void saveSettings(){
        Settings.saveSettings(MainActivity.this,Settings.APP_ACTIVE,swAppOnOff.isChecked());
        Settings.saveSettings(MainActivity.this,Settings.LOCK_TYPE,spLockType.getSelectedItemPosition());
        Settings.saveSettings(MainActivity.this,Settings.TIMER_INTERVAL,spInterval.getSelectedItemPosition());
        Settings.saveSettings(MainActivity.this,Settings.TIMER_INTERVAL_VAL,spInterval.getSelectedItem().toString());
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveSettings();
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveSettings();

    }
}
