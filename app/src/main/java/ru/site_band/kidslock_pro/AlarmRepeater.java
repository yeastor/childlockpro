package ru.site_band.kidslock_pro;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

/**
 * Created by KupriyanovYM on 06.02.2017.
 */

public class AlarmRepeater {
    private static AlarmManager alarmMgr;
    private static PendingIntent alarmIntent;

    public static void setAlarm(Context context)
    {
        Intent intent = new Intent(context,TimerBroadcastReceiver.class);

        //intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

        alarmIntent = PendingIntent.getBroadcast(context, 0,
                intent, 0);


        int alarmType = AlarmManager.ELAPSED_REALTIME;
        final int FIFTEEN_SEC_MILLIS = 15000;
        final int TWO_MINUTES_MILLIS = 120000;
        final int ONE_MINUTES_MILLIS = 60000;

        // The AlarmManager, like most system services, isn't created by application code, but
        // requested from the system.
        alarmMgr = (AlarmManager)
                context.getSystemService(Context.ALARM_SERVICE);

        // setRepeating takes a start delay and period between alarms as arguments.
        // The below code fires after 15 seconds, and repeats every 15 seconds.  This is very
        // useful for demonstration purposes, but horrendous for production.  Don't be that dev.
        alarmMgr.setRepeating(alarmType, SystemClock.elapsedRealtime() + FIFTEEN_SEC_MILLIS,
                ONE_MINUTES_MILLIS, alarmIntent);
        // END_INCLUDE (configure_alarm_manager);

    }

    public static AlarmManager getAlarmMgr()
    {
        return alarmMgr;
    }

    public static void stopAlarm()
    {
        if (alarmMgr != null) {
            alarmMgr.cancel(alarmIntent);
        }
    }
}
