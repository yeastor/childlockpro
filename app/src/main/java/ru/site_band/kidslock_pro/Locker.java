package ru.site_band.kidslock_pro;

/**
 * Created by KupriyanovYM on 15.02.2017.
 */

public interface Locker {
    public QuestionButtonsCollection getButtons();
    public QuestionButtonsCollection redrawButtons(Boolean resize,int screenWidth);
    public int getLetterIndex(int whatWeLearn,String[] letterArray);
    public int[] getOtherLettersIndexes(int letterIndex,String[] letterArray);
    public int[] getBtnIndexes();
    public int getPreviewPhraseResource();
    public int getLetterSoundResource();
    public int getTryMorePhraseResource();
    public int getWhatFindStringResource();

};
