package ru.site_band.kidslock_pro;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by B.E.L on 14/12/2016.
 */

public class BootReceiver extends BroadcastReceiver {
    public BootReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Settings.getSettings(context, Settings.APP_ACTIVE,false)){
            context.startService(new Intent(context, LockScreenService.class));
        }
    }
}
