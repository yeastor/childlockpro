package ru.site_band.kidslock_pro;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.TypedValue;
import android.widget.Button;
import android.widget.GridLayout;

/**
 * Created by KupriyanovYM on 15.02.2017.
 */

class ButtonLocker implements Locker {

    private static int COUNT_BUTTONS_IN_LINE = 4;
    private static int PADDING_BUTTON_PX = 20;

    private Context ctx;
    private int width;
    private int[] btnIndexes;
    private int letterIndex;
    private int type;
    private QuestionButtonsCollection questionButtonsCollection;

    ButtonLocker(Context ctx, int screenWidth, int type){
        this.ctx = ctx;
        this.type = type;
        setButtonWidth(screenWidth);
    }

    private void setButtonWidth(int screenWidth){
        this.width = screenWidth / COUNT_BUTTONS_IN_LINE - PADDING_BUTTON_PX;
    }

    public int[] getBtnIndexes() {
        return btnIndexes;
    }

    public int[] getOtherLettersIndexes(int letterIndex,String[] letterArray){
        int otherLettersCount = 3;
        int newIndex;
        int[] otherLettersIndexes = new int[otherLettersCount + 1];
        int leftLettersCount = Math.round(otherLettersCount / 2);
        int rightLettersCount = otherLettersCount - leftLettersCount;
        for (int i = 0; i<otherLettersCount; i++){
            newIndex = letterIndex + i + 1;
            if (newIndex >= letterArray.length) {
                newIndex = i + 1;
            }
            otherLettersIndexes[i] = newIndex;
        }

        return  otherLettersIndexes;
    }

    public int getLetterSoundResource(){
        TypedArray sounds = this.ctx.getResources().obtainTypedArray(R.array.letter_sound_res);
        return  sounds.getResourceId((letterIndex - 1),0);
    }

    public  int getPreviewPhraseResource(){
        return  R.raw.show_letter;
    }

    public  int getTryMorePhraseResource(){
        TypedArray sounds = this.ctx.getResources().obtainTypedArray(R.array.try_more_arr);
        return  sounds.getResourceId(0,0);
    }

    public int getLetterIndex(int whatWeLearn,String[] letterArray){
        if (whatWeLearn == 0){
            int a = 1; // Начальное значение диапазона - "от"
            int b = letterArray.length - 1; // Конечное значение диапазона - "до"
            letterIndex = a + (int) (Math.random() * b);
            //letter = letterArray[letterIndex];
        }
        else {
            letterIndex = whatWeLearn;
        }

        return letterIndex;
    }
    public QuestionButtonsCollection getButtons(){
        String[] letterArray =  this.ctx.getResources().getStringArray(R.array.abc_what_learn);
        int whatLettersWeLearn = Settings.getSettings(this.ctx,Settings.WHAT_LETTER_LEARN,1);
        letterIndex = getLetterIndex(whatLettersWeLearn,letterArray);
        int[] allLettersIndex = getOtherLettersIndexes(letterIndex,letterArray);
        allLettersIndex[allLettersIndex.length - 1] = letterIndex;
        Helper.shuffleArray(allLettersIndex);
        btnIndexes = new int[allLettersIndex.length];
        questionButtonsCollection = new QuestionButtonsCollection();
        try {
            for (int i = 0;i<allLettersIndex.length;i++){
                String letter = letterArray[allLettersIndex[i]];
                QuestionButton qB = new QuestionButton(this.type,this.ctx);
                qB.setSize(this.width);
                qB.setText(letter);
                Boolean isWrongButton = true;
                if (allLettersIndex[i] == letterIndex) {
                    isWrongButton = false;
                }
                qB.setWrongFlag(isWrongButton);
                qB.setContent(allLettersIndex[i]);
                qB.setId(i);

                btnIndexes[i] = i;
                questionButtonsCollection.add(qB);
            }
        }

        catch (Exception e){
            e.printStackTrace();
        }
        return  questionButtonsCollection;
    }

    public int getWhatFindStringResource(){
        return R.string.find_letter;
    }

    protected void shuffleBtnIndexes(){
        Helper.shuffleArray(this.getBtnIndexes());
    }

    public QuestionButtonsCollection redrawButtons(Boolean resize,int screenWidth){
        this.shuffleBtnIndexes();
        if (resize) {
            this.setButtonWidth(screenWidth);
            for (int i = 0; i < questionButtonsCollection.lenght();i++) {
                questionButtonsCollection.get(i).setSize(this.width);
            }
        }
        return questionButtonsCollection;
    }


}
