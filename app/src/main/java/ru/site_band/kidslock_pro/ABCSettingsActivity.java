package ru.site_band.kidslock_pro;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Spinner;

public class ABCSettingsActivity extends Activity {

    Spinner spWhatLetterLearn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_abcsettings);

        spWhatLetterLearn = (Spinner) findViewById(R.id.spWhatLetterLearn);

        loadSettings();
    }

    private void loadSettings(){
        spWhatLetterLearn.setSelection(Settings.getSettings(this,Settings.WHAT_LETTER_LEARN,1));
    }

    private void saveSettings(){
        Settings.saveSettings(this,Settings.WHAT_LETTER_LEARN,spWhatLetterLearn.getSelectedItemPosition());
        Settings.saveSettings(this,Settings.WHAT_LETTER_LEARN_COUNT,spWhatLetterLearn.getAdapter().getCount());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveSettings();

    }

    @Override
    protected void onPause() {
        super.onPause();
        saveSettings();
    };
}
