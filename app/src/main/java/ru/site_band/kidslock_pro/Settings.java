package ru.site_band.kidslock_pro;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

/**
 * Created by KupriyanovYM on 30.01.2017.
 */

public class Settings {

    public static final String APP_ACTIVE = "app_active";
    public static final String LOCK_TYPE = "lock_type";
    public static final String WHAT_LETTER_LEARN = "what_letter_learn";
    public static final String WHAT_LETTER_LEARN_COUNT = "what_letter_learn_count";
    public static final String PREFERENCES = "main_settings";
    public static final String TIMER_INTERVAL = "timer_interval";
    public static final String TIMER_INTERVAL_VAL = "timer_interval_value";

    public static final int MAX_WRONG_CLICK_COUNT = 2;
    public  static final float FONT_BIG = (float) 2.4;
    public  static final float FONT_MEDIUM = (float) 3.4;
    public  static final float FONT_SMALL = (float) 5;

    @Nullable
    public static int getSettings(Context context, String settingName, @Nullable int defaultValue)    {
        SharedPreferences sharedPref = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        return sharedPref.getInt(settingName, defaultValue);
    }

    public static Boolean getSettings(Context context,String settingName, Boolean defaultValue)    {
        SharedPreferences sharedPref = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(settingName, defaultValue);
    }
    @Nullable
    public static String getSettingsString(Context context,String settingName, @Nullable String defaultValue)    {
        SharedPreferences sharedPref = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        return sharedPref.getString(settingName, defaultValue);
    }

    public static void saveSettings(Context context,String settingName,int value){
        SharedPreferences sharedPref = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(settingName, value);
        editor.apply();
    }

    public static void saveSettings(Context context,String settingName,Boolean value){
        SharedPreferences sharedPref = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(settingName, value);
        editor.apply();
    }

    public static void saveSettings(Context context,String settingName,String value){
        SharedPreferences sharedPref = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(settingName, value);
        editor.apply();
    }
}
