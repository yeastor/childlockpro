package ru.site_band.kidslock_pro;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;

/**
 * Created by KupriyanovYM on 03.02.2017.
 */

public class SoundPlayer {
    public  static void playSoundTwice(Context ctx ,int resourceFirstId, int resourceSecondId){
        //int resID=getResources().getIdentifier("show letter", "sounds", getPackageName());
        //MediaPlayer mediaPlayer1=MediaPlayer.create(ctx,R.raw.show_letter);
        MediaPlayer mediaPlayer1=MediaPlayer.create(ctx,resourceFirstId);
        //mediaPlayer1.setAudioStreamType(AudioManager.STREAM_SYSTEM);
        //int[] intArray =  getResources().getIntArray(R.array.letter_sound_res);
        //sounds = getResources().obtainTypedArray(R.array.letter_sound_res);
        //MediaPlayer mediaPlayer2=MediaPlayer.create(ctx,sounds.getResourceId((letterIndex - 1),0));
        MediaPlayer mediaPlayer2=MediaPlayer.create(ctx,resourceSecondId);
        //mediaPlayer2.setAudioStreamType(AudioManager.STREAM_SYSTEM);

        mediaPlayer1.start();
        mediaPlayer1.setNextMediaPlayer(mediaPlayer2);

    }

    public static void playWrongSound(Context ctx, int resourceId){
        //sounds = getResources().obtainTypedArray(R.array.try_more_arr);
        //MediaPlayer mediaPlayer2 = MediaPlayer.create(ctx,sounds.getResourceId(0,0));
        MediaPlayer mediaPlayer2 = MediaPlayer.create(ctx,resourceId);
        //mediaPlayer2.setAudioStreamType(AudioManager.STREAM_SYSTEM);
        mediaPlayer2.start();
    }
}
