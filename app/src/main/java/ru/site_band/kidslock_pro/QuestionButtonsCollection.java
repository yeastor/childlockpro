package ru.site_band.kidslock_pro;

import java.util.ArrayList;

/**
 * Created by KupriyanovYM on 03.02.2017.
 */

public class QuestionButtonsCollection {

    private ArrayList<QuestionButton> questionButtons = new ArrayList<>();

    public void add(QuestionButton btn){
        questionButtons.add(btn);
    }

    public QuestionButton get(int index){
        return questionButtons.get(index);
    }

    public int lenght(){
        return questionButtons.size();
    }
}
